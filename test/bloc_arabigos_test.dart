import 'package:aprende_maya/aplicacion/bloc_arabigos.dart';
import 'package:aprende_maya/aplicacion/estado_arabigos.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  blocTest<BlocArabigos, EstadoArabigos>(
    'Solo Numero',
    build: () => BlocArabigos(),
    act: (bloc) {
      bloc.cargarNumero(2);
    },
    expect: () => <EstadoArabigos>[
      EstadoArabigos.inicial().copyWith(numero: 2),
    ],
  );
  blocTest<BlocArabigos, EstadoArabigos>(
    'Comprobar Respuesta',
    build: () => BlocArabigos(),
    act: (bloc) {
      bloc.cargarNumero(2);
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 1);
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.agregarPunto();
      expect(bloc.verificarResultado(), true);
    },
    skip: 2,
    expect: () => <EstadoArabigos>[
      EstadoArabigos.inicial()
          .copyWith(numero: 2)
          .copyWith(numerox1: 2)
          .copyWith(seleccionado: 1),
    ],
  );
  blocTest<BlocArabigos, EstadoArabigos>(
    'Comprobar Numero 17',
    build: () => BlocArabigos(),
    act: (bloc) {
      bloc.cargarNumero(17);
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 1);
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.agregarLinea();
      // ignore: cascade_invocations
      bloc.agregarLinea();
      // ignore: cascade_invocations
      bloc.agregarLinea();
    },
    skip: 5,
    expect: () => <EstadoArabigos>[
      EstadoArabigos.inicial()
          .copyWith(numero: 17)
          .copyWith(numerox1: 17)
          .copyWith(seleccionado: 1),
    ],
  );
  blocTest<BlocArabigos, EstadoArabigos>(
    'Comprobar Numero 150',
    build: () => BlocArabigos(),
    act: (bloc) {
      bloc.cargarNumero(150);
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 2);
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.agregarLinea();
      // ignore: cascade_invocations
      bloc.agregarLinea();
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 1);
      // ignore: cascade_invocations
      bloc.agregarLinea();
      // ignore: cascade_invocations
      bloc.agregarLinea();
    },
    skip: 8,
    expect: () => <EstadoArabigos>[
      EstadoArabigos.inicial()
          .copyWith(numero: 150)
          .copyWith(numerox2: 12)
          .copyWith(numerox1: 10)
          .copyWith(seleccionado: 1),
    ],
  );
  blocTest<BlocArabigos, EstadoArabigos>(
    'Comprobar Numero 420',
    build: () => BlocArabigos(),
    act: (bloc) {
      bloc.cargarNumero(420);
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 4);
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 2);
      // ignore: cascade_invocations
      bloc.agregarPunto();
      // ignore: cascade_invocations
      bloc.cambioCasillaSeleccionada(seleccionado: 1);
      // ignore: cascade_invocations
      bloc.agregarCero();
    },
    skip: 6,
    expect: () => <EstadoArabigos>[
      EstadoArabigos.inicial()
          .copyWith(numero: 420)
          .copyWith(numerox4: 1)
          .copyWith(numerox2: 1)
          .copyWith(numerox1: 0)
          .copyWith(seleccionado: 1)
    ],
  );
}
