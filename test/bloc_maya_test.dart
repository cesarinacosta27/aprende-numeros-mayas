import 'package:aprende_maya/aplicacion/bloc_maya.dart';
import 'package:aprende_maya/aplicacion/estado_maya.dart';
import 'package:aprende_maya/aplicacion/simbolos.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  blocTest<BlocMaya, EstadoMaya>(
    'Solo Numero',
    build: () => BlocMaya(),
    act: (bloc) {
      bloc.cargarNumero(2);
    },
    expect: () => <EstadoMaya>[
      EstadoMaya.inicial().copyWith(numero: 2),
    ],
  );
  blocTest<BlocMaya, EstadoMaya>(
    'Comprobar Respuesta',
    build: () => BlocMaya(),
    act: (bloc) {
      bloc.cargarNumero(2);
      // ignore: cascade_invocations
      bloc.cambioRespuesta(respuesta: '2');
      expect(bloc.verificar(), true);
    },
    skip: 1,
    expect: () => <EstadoMaya>[
      EstadoMaya.inicial().copyWith(numero: 2).copyWith(respuesta: '2'),
    ],
  );
  blocTest<BlocMaya, EstadoMaya>(
    'Comprobar Calculo Numeros a Dibujar 17',
    build: () => BlocMaya(),
    act: (bloc) {
      bloc.cargarNumero(17);
      // ignore: cascade_invocations
      bloc.calcularCuadros();
    },
    skip: 4,
    expect: () => <EstadoMaya>[
      EstadoMaya.inicial()
          .copyWith(numero: 17)
          .copyWith(numerox1: 17)
          .copyWith(cajax4: [concha]).copyWith(
        cajax2: [concha],
      ).copyWith(
        cajax1: ['$punto$punto', linea, linea, linea],
      ),
    ],
  );
  blocTest<BlocMaya, EstadoMaya>(
    'Comprobar Calculo Numeros a Dibujar 150',
    build: () => BlocMaya(),
    act: (bloc) {
      bloc.cargarNumero(150);
      // ignore: cascade_invocations
      bloc.calcularCuadros();
    },
    skip: 5,
    expect: () => <EstadoMaya>[
      EstadoMaya.inicial()
          .copyWith(numero: 150)
          .copyWith(numerox2: 7)
          .copyWith(numerox1: 10)
          .copyWith(cajax4: [concha]).copyWith(
        cajax2: ['$punto$punto', linea],
      ).copyWith(
        cajax1: [linea, linea],
      ),
    ],
  );
  blocTest<BlocMaya, EstadoMaya>(
    'Comprobar Calculo Numeros a Dibujar 420',
    build: () => BlocMaya(),
    act: (bloc) {
      bloc.cargarNumero(420);
      // ignore: cascade_invocations
      bloc.calcularCuadros();
    },
    skip: 5,
    expect: () => <EstadoMaya>[
      EstadoMaya.inicial()
          .copyWith(numero: 420)
          .copyWith(numerox4: 1)
          .copyWith(numerox2: 1)
          .copyWith(cajax4: [punto]).copyWith(
        cajax2: [punto],
      ).copyWith(
        cajax1: [concha],
      ),
    ],
  );
}
