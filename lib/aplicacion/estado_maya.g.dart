// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'estado_maya.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EstadoMaya _$_$_EstadoMayaFromJson(Map<String, dynamic> json) {
  return _$_EstadoMaya(
    numero: json['numero'] as int,
    respuesta: json['respuesta'] as String,
    numerox4: json['numerox4'] as int,
    numerox2: json['numerox2'] as int,
    numerox1: json['numerox1'] as int,
    cajax4: (json['cajax4'] as List).map((e) => e as String).toList(),
    cajax2: (json['cajax2'] as List).map((e) => e as String).toList(),
    cajax1: (json['cajax1'] as List).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$_$_EstadoMayaToJson(_$_EstadoMaya instance) =>
    <String, dynamic>{
      'numero': instance.numero,
      'respuesta': instance.respuesta,
      'numerox4': instance.numerox4,
      'numerox2': instance.numerox2,
      'numerox1': instance.numerox1,
      'cajax4': instance.cajax4,
      'cajax2': instance.cajax2,
      'cajax1': instance.cajax1,
    };
