import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'estado_arabigos.dart';
import 'simbolos.dart';

final generadorRandom = Random();

class BlocArabigos extends Cubit<EstadoArabigos> {
  BlocArabigos() : super(EstadoArabigos.inicial());

  void recargar() {
    emit(state.copyWith(numero: generadorRandom.nextInt(401)));
    emit(state.copyWith(numerox4: -1));
    emit(state.copyWith(numerox2: -1));
    emit(state.copyWith(numerox1: -1));
  }

  void cargarNumero(int numeroCargar) {
    emit(state.copyWith(numero: numeroCargar));
  }

  bool verificarResultado() {
    if (state.numero == calcularResultado()) {
      return true;
    } else {
      return false;
    }
  }

  int calcularResultado() {
    var resultado = 0;
    //puntos x4
    if (state.numerox4 != -1) {
      resultado = state.numerox4 * 400;
    }
    if (state.numerox2 != -1 || state.numerox4 != -1) {
      resultado += state.numerox2 * 20;
    }
    if (state.numerox1 != -1 || state.numerox2 != -1 || state.numerox4 != -1) {
      resultado += state.numerox1;
    }
    return resultado;
  }

  void cambioCasillaSeleccionada({required int seleccionado}) {
    emit(
      state.copyWith(seleccionado: seleccionado),
    );
  }

  void agregarCero() {
    if (state.seleccionado != 0) {
      switch (state.seleccionado) {
        case 1:
          emit(state.copyWith(numerox1: 0));
          break;
        case 2:
          emit(state.copyWith(numerox2: 0));
          break;
        case 4:
          emit(state.copyWith(numerox4: 0));
          break;
        default:
      }
    }
  }

  void agregarPunto() {
    if (state.seleccionado != 0) {
      switch (state.seleccionado) {
        case 1:
          if (state.numerox1 < 19) {
            var calculo = 0;
            if (state.numerox1 == -1) {
              calculo = 1;
            }
            calculo += state.numerox1 + 1;
            emit(state.copyWith(numerox1: calculo));
          }
          break;
        case 2:
          if (state.numerox2 < 19) {
            var calculo = 0;
            if (state.numerox2 == -1) {
              calculo = 1;
            }
            calculo += state.numerox2 + 1;
            emit(state.copyWith(numerox2: calculo));
          }
          break;
        case 4:
          if (state.numerox4 < 19) {
            var calculo = 0;
            if (state.numerox4 == -1) {
              calculo = 1;
            }
            calculo += state.numerox4 + 1;
            emit(state.copyWith(numerox4: calculo));
          }
          break;
        default:
      }
    }
  }

  void agregarLinea() {
    if (state.seleccionado != 0) {
      switch (state.seleccionado) {
        case 1:
          if (state.numerox1 < 15) {
            var calculo = 0;
            if (state.numerox1 == -1) {
              calculo = 1;
            }
            calculo += state.numerox1 + 5;
            emit(state.copyWith(numerox1: calculo));
          }
          break;
        case 2:
          if (state.numerox2 < 15) {
            var calculo = 0;
            if (state.numerox2 == -1) {
              calculo = 1;
            }
            calculo += state.numerox2 + 5;
            emit(state.copyWith(numerox2: calculo));
          }
          break;
        case 4:
          if (state.numerox4 < 15) {
            var calculo = 0;
            if (state.numerox4 == -1) {
              calculo = 1;
            }
            calculo += state.numerox4 + 5;
            emit(state.copyWith(numerox4: calculo));
          }
          break;
        default:
      }
    }
  }

  List<String> calcular(int numero) {
    var a = numero;
    final listaRetorno = [''];
    final sb = StringBuffer();
    if (a != 0) {
      if (a > 4) {
        final b = a ~/ 5;
        a = a - (b * 5);
        for (var i = 0; i < a; i++) {
          sb.write(punto);
        }
        listaRetorno[0] = sb.toString();
        for (var i = 0; i < b; i++) {
          listaRetorno.add(linea);
        }
      } else {
        for (var i = 0; i < a; i++) {
          sb.write(punto);
        }
        listaRetorno[0] = sb.toString();
      }
    } else {
      listaRetorno[0] = concha;
    }
    return listaRetorno;
  }
}
