// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'estado_maya.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
EstadoMaya _$EstadoMayaFromJson(Map<String, dynamic> json) {
  return _EstadoMaya.fromJson(json);
}

/// @nodoc
class _$EstadoMayaTearOff {
  const _$EstadoMayaTearOff();

// ignore: unused_element
  _EstadoMaya call(
      {required int numero,
      required String respuesta,
      required int numerox4,
      required int numerox2,
      required int numerox1,
      required List<String> cajax4,
      required List<String> cajax2,
      required List<String> cajax1}) {
    return _EstadoMaya(
      numero: numero,
      respuesta: respuesta,
      numerox4: numerox4,
      numerox2: numerox2,
      numerox1: numerox1,
      cajax4: cajax4,
      cajax2: cajax2,
      cajax1: cajax1,
    );
  }

// ignore: unused_element
  EstadoMaya fromJson(Map<String, Object> json) {
    return EstadoMaya.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $EstadoMaya = _$EstadoMayaTearOff();

/// @nodoc
mixin _$EstadoMaya {
  int get numero;
  String get respuesta;
  int get numerox4;
  int get numerox2;
  int get numerox1;
  List<String> get cajax4;
  List<String> get cajax2;
  List<String> get cajax1;

  Map<String, dynamic> toJson();
  $EstadoMayaCopyWith<EstadoMaya> get copyWith;
}

/// @nodoc
abstract class $EstadoMayaCopyWith<$Res> {
  factory $EstadoMayaCopyWith(
          EstadoMaya value, $Res Function(EstadoMaya) then) =
      _$EstadoMayaCopyWithImpl<$Res>;
  $Res call(
      {int numero,
      String respuesta,
      int numerox4,
      int numerox2,
      int numerox1,
      List<String> cajax4,
      List<String> cajax2,
      List<String> cajax1});
}

/// @nodoc
class _$EstadoMayaCopyWithImpl<$Res> implements $EstadoMayaCopyWith<$Res> {
  _$EstadoMayaCopyWithImpl(this._value, this._then);

  final EstadoMaya _value;
  // ignore: unused_field
  final $Res Function(EstadoMaya) _then;

  @override
  $Res call({
    Object numero = freezed,
    Object respuesta = freezed,
    Object numerox4 = freezed,
    Object numerox2 = freezed,
    Object numerox1 = freezed,
    Object cajax4 = freezed,
    Object cajax2 = freezed,
    Object cajax1 = freezed,
  }) {
    return _then(_value.copyWith(
      numero: numero == freezed ? _value.numero : numero as int,
      respuesta: respuesta == freezed ? _value.respuesta : respuesta as String,
      numerox4: numerox4 == freezed ? _value.numerox4 : numerox4 as int,
      numerox2: numerox2 == freezed ? _value.numerox2 : numerox2 as int,
      numerox1: numerox1 == freezed ? _value.numerox1 : numerox1 as int,
      cajax4: cajax4 == freezed ? _value.cajax4 : cajax4 as List<String>,
      cajax2: cajax2 == freezed ? _value.cajax2 : cajax2 as List<String>,
      cajax1: cajax1 == freezed ? _value.cajax1 : cajax1 as List<String>,
    ));
  }
}

/// @nodoc
abstract class _$EstadoMayaCopyWith<$Res> implements $EstadoMayaCopyWith<$Res> {
  factory _$EstadoMayaCopyWith(
          _EstadoMaya value, $Res Function(_EstadoMaya) then) =
      __$EstadoMayaCopyWithImpl<$Res>;
  @override
  $Res call(
      {int numero,
      String respuesta,
      int numerox4,
      int numerox2,
      int numerox1,
      List<String> cajax4,
      List<String> cajax2,
      List<String> cajax1});
}

/// @nodoc
class __$EstadoMayaCopyWithImpl<$Res> extends _$EstadoMayaCopyWithImpl<$Res>
    implements _$EstadoMayaCopyWith<$Res> {
  __$EstadoMayaCopyWithImpl(
      _EstadoMaya _value, $Res Function(_EstadoMaya) _then)
      : super(_value, (v) => _then(v as _EstadoMaya));

  @override
  _EstadoMaya get _value => super._value as _EstadoMaya;

  @override
  $Res call({
    Object numero = freezed,
    Object respuesta = freezed,
    Object numerox4 = freezed,
    Object numerox2 = freezed,
    Object numerox1 = freezed,
    Object cajax4 = freezed,
    Object cajax2 = freezed,
    Object cajax1 = freezed,
  }) {
    return _then(_EstadoMaya(
      numero: numero == freezed ? _value.numero : numero as int,
      respuesta: respuesta == freezed ? _value.respuesta : respuesta as String,
      numerox4: numerox4 == freezed ? _value.numerox4 : numerox4 as int,
      numerox2: numerox2 == freezed ? _value.numerox2 : numerox2 as int,
      numerox1: numerox1 == freezed ? _value.numerox1 : numerox1 as int,
      cajax4: cajax4 == freezed ? _value.cajax4 : cajax4 as List<String>,
      cajax2: cajax2 == freezed ? _value.cajax2 : cajax2 as List<String>,
      cajax1: cajax1 == freezed ? _value.cajax1 : cajax1 as List<String>,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_EstadoMaya implements _EstadoMaya {
  _$_EstadoMaya(
      {required this.numero,
      required this.respuesta,
      required this.numerox4,
      required this.numerox2,
      required this.numerox1,
      required this.cajax4,
      required this.cajax2,
      required this.cajax1})
      : assert(numero != null),
        assert(respuesta != null),
        assert(numerox4 != null),
        assert(numerox2 != null),
        assert(numerox1 != null),
        assert(cajax4 != null),
        assert(cajax2 != null),
        assert(cajax1 != null);

  factory _$_EstadoMaya.fromJson(Map<String, dynamic> json) =>
      _$_$_EstadoMayaFromJson(json);

  @override
  final int numero;
  @override
  final String respuesta;
  @override
  final int numerox4;
  @override
  final int numerox2;
  @override
  final int numerox1;
  @override
  final List<String> cajax4;
  @override
  final List<String> cajax2;
  @override
  final List<String> cajax1;

  @override
  String toString() {
    return 'EstadoMaya(numero: $numero, respuesta: $respuesta, numerox4: $numerox4, numerox2: $numerox2, numerox1: $numerox1, cajax4: $cajax4, cajax2: $cajax2, cajax1: $cajax1)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EstadoMaya &&
            (identical(other.numero, numero) ||
                const DeepCollectionEquality().equals(other.numero, numero)) &&
            (identical(other.respuesta, respuesta) ||
                const DeepCollectionEquality()
                    .equals(other.respuesta, respuesta)) &&
            (identical(other.numerox4, numerox4) ||
                const DeepCollectionEquality()
                    .equals(other.numerox4, numerox4)) &&
            (identical(other.numerox2, numerox2) ||
                const DeepCollectionEquality()
                    .equals(other.numerox2, numerox2)) &&
            (identical(other.numerox1, numerox1) ||
                const DeepCollectionEquality()
                    .equals(other.numerox1, numerox1)) &&
            (identical(other.cajax4, cajax4) ||
                const DeepCollectionEquality().equals(other.cajax4, cajax4)) &&
            (identical(other.cajax2, cajax2) ||
                const DeepCollectionEquality().equals(other.cajax2, cajax2)) &&
            (identical(other.cajax1, cajax1) ||
                const DeepCollectionEquality().equals(other.cajax1, cajax1)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(numero) ^
      const DeepCollectionEquality().hash(respuesta) ^
      const DeepCollectionEquality().hash(numerox4) ^
      const DeepCollectionEquality().hash(numerox2) ^
      const DeepCollectionEquality().hash(numerox1) ^
      const DeepCollectionEquality().hash(cajax4) ^
      const DeepCollectionEquality().hash(cajax2) ^
      const DeepCollectionEquality().hash(cajax1);

  @override
  _$EstadoMayaCopyWith<_EstadoMaya> get copyWith =>
      __$EstadoMayaCopyWithImpl<_EstadoMaya>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_EstadoMayaToJson(this);
  }
}

abstract class _EstadoMaya implements EstadoMaya {
  factory _EstadoMaya(
      {required int numero,
      required String respuesta,
      required int numerox4,
      required int numerox2,
      required int numerox1,
      required List<String> cajax4,
      required List<String> cajax2,
      required List<String> cajax1}) = _$_EstadoMaya;

  factory _EstadoMaya.fromJson(Map<String, dynamic> json) =
      _$_EstadoMaya.fromJson;

  @override
  int get numero;
  @override
  String get respuesta;
  @override
  int get numerox4;
  @override
  int get numerox2;
  @override
  int get numerox1;
  @override
  List<String> get cajax4;
  @override
  List<String> get cajax2;
  @override
  List<String> get cajax1;
  @override
  _$EstadoMayaCopyWith<_EstadoMaya> get copyWith;
}
