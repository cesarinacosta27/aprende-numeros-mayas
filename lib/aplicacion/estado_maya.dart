import 'dart:math';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'estado_maya.freezed.dart';
part 'estado_maya.g.dart';

final generadorRandom = Random();

@freezed
abstract class EstadoMaya with _$EstadoMaya {
  factory EstadoMaya({
    required int numero,
    required String respuesta,
    required int numerox4,
    required int numerox2,
    required int numerox1,
    required List<String> cajax4,
    required List<String> cajax2,
    required List<String> cajax1,
  }) = _EstadoMaya;

  factory EstadoMaya.inicial() => EstadoMaya(
        numero: generadorRandom.nextInt(401),
        respuesta: '',
        numerox4: 0,
        numerox2: 0,
        numerox1: 0,
        cajax4: [],
        cajax2: [],
        cajax1: [],
      );

  factory EstadoMaya.fromJson(Map<String, dynamic> json) =>
      _$EstadoMayaFromJson(json);
}
