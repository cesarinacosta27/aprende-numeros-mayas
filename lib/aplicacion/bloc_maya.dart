import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'estado_maya.dart';
import 'simbolos.dart';

final generadorRandom = Random();

class BlocMaya extends Cubit<EstadoMaya> {
  BlocMaya() : super(EstadoMaya.inicial());
  void recargar() {
    emit(state.copyWith(numero: generadorRandom.nextInt(401)));
  }

  void cargarNumero(int numeroInsertar) {
    emit(state.copyWith(numero: numeroInsertar));
  }

  void cambioRespuesta({required String respuesta}) {
    emit(state.copyWith(respuesta: respuesta));
  }

  bool verificar() {
    if (state.numero == int.parse(state.respuesta)) {
      return true;
    } else {
      return false;
    }
  }

  void calcularCuadros() {
    var numeroM = state.numero;
    if (numeroM > 399) {
      final a = numeroM ~/ 400;
      emit(state.copyWith(numerox4: a));
      emit(state.copyWith(cajax4: calcularSimbolos(a)));
      numeroM = numeroM - (a * 400);
    } else {
      emit(state.copyWith(cajax4: calcularSimbolos(0)));
    }
    if (numeroM > 19) {
      final a = numeroM ~/ 20;
      emit(state.copyWith(numerox2: a));
      emit(state.copyWith(cajax2: calcularSimbolos(a)));
      numeroM = numeroM - (a * 20);
    } else {
      emit(state.copyWith(cajax2: calcularSimbolos(0)));
    }
    if (numeroM > 0) {
      emit(state.copyWith(numerox1: numeroM));
      emit(state.copyWith(cajax1: calcularSimbolos(numeroM)));
    } else {
      emit(state.copyWith(cajax1: calcularSimbolos(0)));
    }
  }

  List<String> calcularSimbolos(int numero) {
    var a = numero;
    final listaRetorno = [''];
    final sb = StringBuffer();
    if (a != 0) {
      if (a > 4) {
        final b = a ~/ 5;
        a = a - (b * 5);
        for (var i = 0; i < a; i++) {
          sb.write(punto);
        }
        if (sb.isNotEmpty) {
          listaRetorno[0] = sb.toString();
        }
        for (var i = 0; i < b; i++) {
          if (listaRetorno[0] == '') {
            listaRetorno[0] = linea;
          } else {
            listaRetorno.add(linea);
          }
        }
      } else {
        for (var i = 0; i < a; i++) {
          sb.write(punto);
        }
        listaRetorno[0] = sb.toString();
      }
    } else {
      listaRetorno[0] = concha;
    }
    return listaRetorno;
  }
}
