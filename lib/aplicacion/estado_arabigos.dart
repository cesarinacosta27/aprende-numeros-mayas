import 'dart:math';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'estado_arabigos.freezed.dart';
part 'estado_arabigos.g.dart';

final generadorRandom = Random();

@freezed
abstract class EstadoArabigos with _$EstadoArabigos {
  factory EstadoArabigos({
    required int numero,
    required int numerox4,
    required int numerox2,
    required int numerox1,
    required int seleccionado,
  }) = _EstadoArabigos;
  factory EstadoArabigos.inicial() => EstadoArabigos(
        numero: generadorRandom.nextInt(401),
        numerox4: -1,
        numerox2: -1,
        numerox1: -1,
        seleccionado: 1,
      );
  factory EstadoArabigos.fromJson(Map<String, dynamic> json) =>
      _$EstadoArabigosFromJson(json);
}
