// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'estado_arabigos.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
EstadoArabigos _$EstadoArabigosFromJson(Map<String, dynamic> json) {
  return _EstadoArabigos.fromJson(json);
}

/// @nodoc
class _$EstadoArabigosTearOff {
  const _$EstadoArabigosTearOff();

// ignore: unused_element
  _EstadoArabigos call(
      {required int numero,
      required int numerox4,
      required int numerox2,
      required int numerox1,
      required int seleccionado}) {
    return _EstadoArabigos(
      numero: numero,
      numerox4: numerox4,
      numerox2: numerox2,
      numerox1: numerox1,
      seleccionado: seleccionado,
    );
  }

// ignore: unused_element
  EstadoArabigos fromJson(Map<String, Object> json) {
    return EstadoArabigos.fromJson(json);
  }
}

/// @nodoc
// ignore: unused_element
const $EstadoArabigos = _$EstadoArabigosTearOff();

/// @nodoc
mixin _$EstadoArabigos {
  int get numero;
  int get numerox4;
  int get numerox2;
  int get numerox1;
  int get seleccionado;

  Map<String, dynamic> toJson();
  $EstadoArabigosCopyWith<EstadoArabigos> get copyWith;
}

/// @nodoc
abstract class $EstadoArabigosCopyWith<$Res> {
  factory $EstadoArabigosCopyWith(
          EstadoArabigos value, $Res Function(EstadoArabigos) then) =
      _$EstadoArabigosCopyWithImpl<$Res>;
  $Res call(
      {int numero, int numerox4, int numerox2, int numerox1, int seleccionado});
}

/// @nodoc
class _$EstadoArabigosCopyWithImpl<$Res>
    implements $EstadoArabigosCopyWith<$Res> {
  _$EstadoArabigosCopyWithImpl(this._value, this._then);

  final EstadoArabigos _value;
  // ignore: unused_field
  final $Res Function(EstadoArabigos) _then;

  @override
  $Res call({
    Object numero = freezed,
    Object numerox4 = freezed,
    Object numerox2 = freezed,
    Object numerox1 = freezed,
    Object seleccionado = freezed,
  }) {
    return _then(_value.copyWith(
      numero: numero == freezed ? _value.numero : numero as int,
      numerox4: numerox4 == freezed ? _value.numerox4 : numerox4 as int,
      numerox2: numerox2 == freezed ? _value.numerox2 : numerox2 as int,
      numerox1: numerox1 == freezed ? _value.numerox1 : numerox1 as int,
      seleccionado:
          seleccionado == freezed ? _value.seleccionado : seleccionado as int,
    ));
  }
}

/// @nodoc
abstract class _$EstadoArabigosCopyWith<$Res>
    implements $EstadoArabigosCopyWith<$Res> {
  factory _$EstadoArabigosCopyWith(
          _EstadoArabigos value, $Res Function(_EstadoArabigos) then) =
      __$EstadoArabigosCopyWithImpl<$Res>;
  @override
  $Res call(
      {int numero, int numerox4, int numerox2, int numerox1, int seleccionado});
}

/// @nodoc
class __$EstadoArabigosCopyWithImpl<$Res>
    extends _$EstadoArabigosCopyWithImpl<$Res>
    implements _$EstadoArabigosCopyWith<$Res> {
  __$EstadoArabigosCopyWithImpl(
      _EstadoArabigos _value, $Res Function(_EstadoArabigos) _then)
      : super(_value, (v) => _then(v as _EstadoArabigos));

  @override
  _EstadoArabigos get _value => super._value as _EstadoArabigos;

  @override
  $Res call({
    Object numero = freezed,
    Object numerox4 = freezed,
    Object numerox2 = freezed,
    Object numerox1 = freezed,
    Object seleccionado = freezed,
  }) {
    return _then(_EstadoArabigos(
      numero: numero == freezed ? _value.numero : numero as int,
      numerox4: numerox4 == freezed ? _value.numerox4 : numerox4 as int,
      numerox2: numerox2 == freezed ? _value.numerox2 : numerox2 as int,
      numerox1: numerox1 == freezed ? _value.numerox1 : numerox1 as int,
      seleccionado:
          seleccionado == freezed ? _value.seleccionado : seleccionado as int,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_EstadoArabigos implements _EstadoArabigos {
  _$_EstadoArabigos(
      {required this.numero,
      required this.numerox4,
      required this.numerox2,
      required this.numerox1,
      required this.seleccionado})
      : assert(numero != null),
        assert(numerox4 != null),
        assert(numerox2 != null),
        assert(numerox1 != null),
        assert(seleccionado != null);

  factory _$_EstadoArabigos.fromJson(Map<String, dynamic> json) =>
      _$_$_EstadoArabigosFromJson(json);

  @override
  final int numero;
  @override
  final int numerox4;
  @override
  final int numerox2;
  @override
  final int numerox1;
  @override
  final int seleccionado;

  @override
  String toString() {
    return 'EstadoArabigos(numero: $numero, numerox4: $numerox4, numerox2: $numerox2, numerox1: $numerox1, seleccionado: $seleccionado)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EstadoArabigos &&
            (identical(other.numero, numero) ||
                const DeepCollectionEquality().equals(other.numero, numero)) &&
            (identical(other.numerox4, numerox4) ||
                const DeepCollectionEquality()
                    .equals(other.numerox4, numerox4)) &&
            (identical(other.numerox2, numerox2) ||
                const DeepCollectionEquality()
                    .equals(other.numerox2, numerox2)) &&
            (identical(other.numerox1, numerox1) ||
                const DeepCollectionEquality()
                    .equals(other.numerox1, numerox1)) &&
            (identical(other.seleccionado, seleccionado) ||
                const DeepCollectionEquality()
                    .equals(other.seleccionado, seleccionado)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(numero) ^
      const DeepCollectionEquality().hash(numerox4) ^
      const DeepCollectionEquality().hash(numerox2) ^
      const DeepCollectionEquality().hash(numerox1) ^
      const DeepCollectionEquality().hash(seleccionado);

  @override
  _$EstadoArabigosCopyWith<_EstadoArabigos> get copyWith =>
      __$EstadoArabigosCopyWithImpl<_EstadoArabigos>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_EstadoArabigosToJson(this);
  }
}

abstract class _EstadoArabigos implements EstadoArabigos {
  factory _EstadoArabigos(
      {required int numero,
      required int numerox4,
      required int numerox2,
      required int numerox1,
      required int seleccionado}) = _$_EstadoArabigos;

  factory _EstadoArabigos.fromJson(Map<String, dynamic> json) =
      _$_EstadoArabigos.fromJson;

  @override
  int get numero;
  @override
  int get numerox4;
  @override
  int get numerox2;
  @override
  int get numerox1;
  @override
  int get seleccionado;
  @override
  _$EstadoArabigosCopyWith<_EstadoArabigos> get copyWith;
}
