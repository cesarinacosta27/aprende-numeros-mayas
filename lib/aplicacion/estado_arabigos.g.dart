// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'estado_arabigos.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EstadoArabigos _$_$_EstadoArabigosFromJson(Map<String, dynamic> json) {
  return _$_EstadoArabigos(
    numero: json['numero'] as int,
    numerox4: json['numerox4'] as int,
    numerox2: json['numerox2'] as int,
    numerox1: json['numerox1'] as int,
    seleccionado: json['seleccionado'] as int,
  );
}

Map<String, dynamic> _$_$_EstadoArabigosToJson(_$_EstadoArabigos instance) =>
    <String, dynamic>{
      'numero': instance.numero,
      'numerox4': instance.numerox4,
      'numerox2': instance.numerox2,
      'numerox1': instance.numerox1,
      'seleccionado': instance.seleccionado,
    };
