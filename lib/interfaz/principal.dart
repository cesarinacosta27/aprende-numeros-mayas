import 'package:flutter/material.dart';

import 'arabigos.dart';
import 'comunes.dart';
import 'maya.dart';

class WidgetInicial extends StatelessWidget {
  const WidgetInicial({
    required this.titulo,
    Key? key,
  }) : super(key: key);
  final String titulo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titulo),
      ),
      body: Ink(
        decoration: decoracionFondo,
        child: Center(
          child: Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
              border: Border.all(
                width: 2,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const Maya(
                            key: Key('Maya'),
                          ),
                        ),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(15),
                    ),
                    child: Text(
                      'Maya a Arábigos',
                      style: estiloComun,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const Arabigos(
                            key: Key('Arabigos'),
                          ),
                        ),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(15),
                    ),
                    child: Text(
                      'Arábigos a Maya',
                      style: estiloComun,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
