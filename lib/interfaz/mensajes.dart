import 'package:flutter/material.dart';

class Dialogo extends StatelessWidget {
  const Dialogo({
    required this.titulo,
    required this.contenido,
    required this.error,
    required Key key,
  }) : super(key: key);

  final String titulo;
  final String contenido;
  final bool error;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: cualColor(error: error),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
            ),
            child: CualIcono(
              error: error,
              key: const Key('CualIcono'),
            ),
          ),
          Column(
            children: [
              Text(titulo),
              const SizedBox(
                height: 5,
              ),
              Text(contenido),
              const SizedBox(
                height: 10,
              ),
              Container(
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      color: Colors.blue,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Center(
                        child: Text('Aceptar'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

Color cualColor({required bool error}) {
  if (error) {
    return Colors.red;
  } else {
    return Colors.green;
  }
}

class CualIcono extends StatelessWidget {
  const CualIcono({
    required this.error,
    required Key key,
  }) : super(key: key);
  final bool error;
  @override
  Widget build(BuildContext context) {
    if (error) {
      return Container(
        height: 150,
        width: 150,
        child: const Icon(Icons.error),
      );
    } else {
      return Container(
        height: 150,
        width: 150,
        child: const Icon(Icons.check),
      );
    }
  }
}
