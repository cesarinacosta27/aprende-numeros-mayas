import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../aplicacion/bloc_arabigos.dart';
import '../aplicacion/estado_arabigos.dart';
import '../aplicacion/simbolos.dart';
import 'comunes.dart';

class Arabigos extends StatelessWidget {
  const Arabigos({required Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BlocArabigos(),
      child: MaterialApp(
        title: 'Aprende Maya',
        debugShowCheckedModeBanner: false,
        theme: temaGeneral,
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Arábigos'),
            leading: BackButton(
              color: Colors.white,
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: BlocBuilder<BlocArabigos, EstadoArabigos>(
            builder: (contextoo, estado) {
              return Ink(
                decoration: decoracionFondo,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Center(
                        child: Card(
                      color: Colors.amber,
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: Text(
                          estado.numero.toString(),
                          style: const TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        const CajaNumerosMayasInput(
                          key: Key('CajaArabigos'),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: ElevatedButton(
                                onPressed: () {
                                  contextoo.read<BlocArabigos>().agregarCero();
                                },
                                child: Text(
                                  concha,
                                  style: estiloComun,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: ElevatedButton(
                                onPressed: () {
                                  contextoo.read<BlocArabigos>().agregarPunto();
                                },
                                child: Text(
                                  punto,
                                  style: estiloComun,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: ElevatedButton(
                                onPressed: () {
                                  contextoo.read<BlocArabigos>().agregarLinea();
                                },
                                child: Text(
                                  linea,
                                  style: estiloComun,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: ElevatedButton(
                              onPressed: () {
                                if (contextoo
                                    .read<BlocArabigos>()
                                    .verificarResultado()) {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return dialogoCorrecto;
                                    },
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return dialogoError;
                                    },
                                  );
                                }
                              },
                              child: Text(
                                'Verificar',
                                style: estiloComun,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          ),
          floatingActionButton: BlocBuilder<BlocArabigos, EstadoArabigos>(
            builder: (context, state) {
              return FloatingActionButton(
                onPressed: () {
                  context.read<BlocArabigos>().recargar();
                },
                child: const Icon(Icons.refresh),
              );
            },
          ),
        ),
      ),
    );
  }
}

class CajaNumerosMayasInput extends StatelessWidget {
  const CajaNumerosMayasInput({
    required Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    const alturaRectangulo = 66.0;
    const anchuraRectangulo = 100.0;
    return BlocBuilder<BlocArabigos, EstadoArabigos>(
        builder: (contextoo, estado) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              contextoo
                  .read<BlocArabigos>()
                  .cambioCasillaSeleccionada(seleccionado: 4);
            },
            child: Container(
              width: anchuraRectangulo,
              height: alturaRectangulo,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                ),
                color: cualColorNumero(
                    seleccionado: estado.seleccionado, cualEs: 4),
              ),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  for (final simbolo in contextoo
                      .watch<BlocArabigos>()
                      .calcular(estado.numerox4))
                    Expanded(
                      child: Text(simbolo),
                    ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              contextoo
                  .read<BlocArabigos>()
                  .cambioCasillaSeleccionada(seleccionado: 2);
            },
            child: Container(
              width: anchuraRectangulo,
              height: alturaRectangulo,
              decoration: BoxDecoration(
                border: const Border(
                  right: BorderSide(
                    width: 2,
                  ),
                  left: BorderSide(
                    width: 2,
                  ),
                ),
                color: cualColorNumero(
                    seleccionado: estado.seleccionado, cualEs: 2),
              ),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  for (final simbolo in contextoo
                      .watch<BlocArabigos>()
                      .calcular(estado.numerox2))
                    Expanded(
                      child: Text(simbolo),
                    ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              contextoo
                  .read<BlocArabigos>()
                  .cambioCasillaSeleccionada(seleccionado: 1);
            },
            child: Container(
              width: anchuraRectangulo,
              height: alturaRectangulo,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                ),
                color: cualColorNumero(
                    seleccionado: estado.seleccionado, cualEs: 1),
              ),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  for (final simbolo in contextoo
                      .watch<BlocArabigos>()
                      .calcular(estado.numerox1))
                    Expanded(
                      child: Text(simbolo),
                    ),
                ],
              ),
            ),
          ),
        ],
      );
    });
  }
}
