import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../aplicacion/bloc_maya.dart';
import '../aplicacion/estado_maya.dart';
import '../aplicacion/simbolos.dart';
import 'comunes.dart';

class Maya extends StatelessWidget {
  const Maya({required Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _controladorTexto = TextEditingController();
    return BlocProvider(
      create: (context) => BlocMaya(),
      child: MaterialApp(
        title: 'Aprende Maya',
        debugShowCheckedModeBanner: false,
        theme: temaGeneral,
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Mayas'),
            leading: BackButton(
              color: Colors.white,
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: BlocBuilder<BlocMaya, EstadoMaya>(
            builder: (contextoo, estado) {
              contextoo.watch<BlocMaya>().calcularCuadros();
              return Ink(
                decoration: decoracionFondo,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    const Center(
                      child: CajaNumerosMayas(
                        key: Key('keyCaja'),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            width: 200,
                            height: 100,
                            child: TextFormField(
                              onChanged: (value) {
                                contextoo
                                    .read<BlocMaya>()
                                    .cambioRespuesta(respuesta: value);
                              },
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly
                              ],
                              controller: _controladorTexto,
                              decoration: InputDecoration(
                                labelText: 'Respuesta',
                                floatingLabelStyle:
                                    const TextStyle(color: Colors.white),
                                fillColor: Colors.white,
                                focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      const BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                border: OutlineInputBorder(
                                  borderSide:
                                      const BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(25),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: ElevatedButton(
                            onPressed: () {
                              if (contextoo.read<BlocMaya>().verificar()) {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return dialogoCorrecto;
                                  },
                                );
                              } else {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return dialogoError;
                                  },
                                );
                              }
                            },
                            child: Text(
                              'Verificar',
                              style: estiloComun,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              );
            },
          ),
          floatingActionButton: BlocBuilder<BlocMaya, EstadoMaya>(
            builder: (context, state) {
              return FloatingActionButton(
                onPressed: () {
                  context.read<BlocMaya>().recargar();
                  _controladorTexto.clear();
                },
                child: const Icon(Icons.refresh),
              );
            },
          ),
        ),
      ),
    );
  }
}

class CajaNumerosMayas extends StatelessWidget {
  const CajaNumerosMayas({
    required Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const alturaRectangulo = 66.0;
    const anchuraRectangulo = 100.0;

    return BlocBuilder<BlocMaya, EstadoMaya>(
      builder: (contextoo, estado) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (estado.cajax4[0] != concha)
              Container(
                width: anchuraRectangulo,
                height: alturaRectangulo,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                  ),
                ),
                child: Flex(
                  direction: Axis.vertical,
                  children: [
                    for (final simbolo in estado.cajax4)
                      Expanded(
                        child: Text(simbolo),
                      ),
                  ],
                ),
              ),
            if (estado.cajax2[0] != concha || estado.cajax4[0] != concha)
              Container(
                width: anchuraRectangulo,
                height: alturaRectangulo,
                decoration:
                    BoxDecoration(border: superiorOno(estado.cajax4[0])),
                child: Flex(
                  direction: Axis.vertical,
                  children: [
                    for (final simbolo in estado.cajax2)
                      Expanded(
                        child: Text(simbolo),
                      ),
                  ],
                ),
              ),
            Container(
              width: anchuraRectangulo,
              height: alturaRectangulo,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                ),
              ),
              child: Flex(
                direction: Axis.vertical,
                children: [
                  for (final simbolo in estado.cajax1)
                    Expanded(
                      child: Text(simbolo),
                    ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}

Border superiorOno(String simbolo) {
  if (simbolo != concha) {
    return const Border(
      right: BorderSide(
        width: 2,
      ),
      left: BorderSide(
        width: 2,
      ),
    );
  } else {
    return const Border(
      right: BorderSide(
        width: 2,
      ),
      left: BorderSide(
        width: 2,
      ),
      top: BorderSide(
        width: 2,
      ),
    );
  }
}
