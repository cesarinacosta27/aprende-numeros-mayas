import 'package:flutter/material.dart';
import 'mensajes.dart';

Decoration decoracionFondo = BoxDecoration(
  gradient: LinearGradient(
    colors: [Colors.blue, colorAzul],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  ),
);

MaterialColor get colorAzul => Colors.blue;
ThemeData temaGeneral = ThemeData(
  primaryColor: Colors.blue,
  buttonTheme: ButtonThemeData(
    buttonColor: Colors.blue[600],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18),
      side: const BorderSide(color: Colors.blue),
    ),
  ),
);

Widget dialogoError = const Dialogo(
  titulo: 'Error',
  contenido: 'La respuesta fue incorrecta',
  error: true,
  key: Key('Dialogo'),
);
Widget dialogoCorrecto = const Dialogo(
  titulo: 'Acertado',
  contenido: 'La respuesta fue correcta',
  error: false,
  key: Key('dialogoSuccess'),
);
TextStyle estiloComun = const TextStyle(
  fontSize: 20,
);

Color cualColorNumero({required int seleccionado, required int cualEs}) {
  if (seleccionado == cualEs) {
    return Colors.blue;
  } else {
    return Colors.white;
  }
}
