import 'package:flutter/material.dart';

import 'interfaz/comunes.dart';
import 'interfaz/principal.dart';

void main() {
  runApp(const MiApp(
    key: Key('PRRUEBA'),
  ));
}

class MiApp extends StatelessWidget {
  const MiApp({required Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aprende Maya',
      debugShowCheckedModeBanner: false,
      theme: temaGeneral,
      home: const WidgetInicial(titulo: 'Aprende Numeros Mayas'),
    );
  }
}
